#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int RTFtoTXT(char *fname,char *target)
{
    printf("filename=%s\n",fname);
    FILE *f1;
    FILE *f2;
    if( (f1=fopen(fname,"r")) == 0 )
    {
     printf(" 1 can't open source file\n");
     exit(1);
    }
    if( (f2=fopen(target,"w")) == 0 )
    {
     printf("1 can't open target file\n");
     exit(1);
    }
    char *str, *buf, *constr;
    str=malloc(3000);
    buf=malloc(3000);
    constr=malloc(100);
    char *i,*j,*k;
    int l,p=0,p1=0;
    while(fgets(str,3000,f1)!=NULL)
    {
        p1++;
        //printf("\nstr=%i",p1);
        i=str;
        if(p==0)
        {
            if((k=strstr(i,"\pard"))==NULL)
              {
                  continue;
              }
        }
        p=1;
        if((*i=='\\')||(*i=='{'))
        {
            i=strchr(str,' ');
            if((i==NULL))
            {
                continue;
            }
        }
        j=strchr(i,'\\');
        if(j==NULL)
        {
            continue;
        }
        while(1)
        {
            //if(p1==12){printf("%s\n%s\n%s\n_________\n",i,j,buf);}
            l=j-i-1;
            memset(buf,0,3000);
            if (*i==' '){strncpy(buf,i+1,l);}
            else {strncpy(buf,i,l+1);}
            fprintf(f2,"%s",buf);

            //if(p1==12){printf("%s\n%s\n%s\n_________\n",i,j,buf);}
            i=strchr(j,' ');
            if(i==NULL)
            {
                fprintf(f2,"\n");
                break;
            }
            memset(constr,0,100);
            strncpy(constr,j,i-j);
            //printf("constr=%s\n",constr);
            RtfToTxt_constr(constr,f2);
            j=strchr(i,'\\');
        }
        fprintf(f2,"\n");
        //printf("%s\n%s\n%s\n_________\n",i,j,buf);
    }
    free(buf);
    free(str);
    free(constr);
    fclose(f1);
    fclose(f2);
    return 0;
}
int RtfToTxt_constr(char *constr, FILE *f2)
{
    int l=strlen(constr);
    char *i,*j;
    char buf[100];
    FILE *f3=fopen("constr.txt","r");
    while(fgets(buf,100,f3)!=NULL)
    {
        if((strstr(buf,constr))!=NULL)
        {
            fprintf(f2,"%s",buf+l+1);
        }
    }
    return 0;
}
