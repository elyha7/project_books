#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <unistd.h>
int FB2_getauthor(char *str,FILE *f1, FILE *f2 );
int FB2_gettitle(char *str,FILE *f2 );
int FB2_parse(char *str,FILE *f1, FILE *f2 );
int FB2_constr(char *str);

int FB2toTXT(char *fname,char *target)
{
    printf("filename=%s\n",fname);
    FILE *f1;
    FILE *f2;
    if( (f1=fopen(fname,"r")) == 0 )
    {
     printf(" 1 can't open source file\n");
     exit(1);
    }
    if( (f2=fopen(target,"w")) == 0 )
    {
     printf("1 can't open target file\n");
     exit(1);
    }
    char *str,*constr;
    str=malloc(3000);
    constr=malloc(100);
    char *i,*j,*k;
    int l,p=0,p1=0,author=0,title=0;
    do
    {
        fgets(str,3000,f1);
        //printf("%s\n___________\n",str);
        if (author==0)
        {
            if((i=strstr(str,"<author>"))!=NULL)
            {
                FB2_getauthor(str,f1,f2);
                author=1;
                continue;
            }
        }
        if (title==0)
        {
            if((i=strstr(str,"<book-title>"))!=NULL)
            {
                FB2_gettitle(str,f2);
                title=1;
                continue;
            }
        }
        if(strstr(str,"<body>")!=NULL)
        {
            FB2_parse(str,f1,f2);
        }
        p1++;
    }while((strstr(str,"</body>"))==NULL);
    free(str);
    free(constr);
    fclose(f1);
    fclose(f2);
    return 0;
}

int FB2_getauthor(char *str,FILE *f1, FILE *f2 )
{
    char *i,*j;
    char buff[50],tofile[100];
    sprintf(tofile,"Author: ");
    memset(buff,0,50);
    while((strstr(str,"</author>"))==NULL)
    {
        fgets(str,2000,f1);
        if((i=strstr(str,"<first-name>"))!=NULL)
        {
            if((j=strstr(i,"</first-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s ",tofile,buff);
                memset(buff,0,50);
            }
        }
        if((i=strstr(str,"<middle-name>"))!=NULL)
        {
            if((j=strstr(i,"</middle-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s ",tofile,buff);
                memset(buff,0,50);
            }
        }
        if((i=strstr(str,"<last-name>"))!=NULL)
        {
            if((j=strstr(i,"</last-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s",tofile,buff);
                memset(buff,0,50);
            }
        }
    }
    if((strlen(tofile))>10)
        fprintf(f2,"%s\n",tofile);
    return 0;
}
int FB2_gettitle(char *str,FILE *f2 )
{
    char *i,*j;
    char buff[100];
    memset(buff,0,100);
    if((j=strstr(str,"</book-title>"))!=NULL)
    {
                i=strchr(str,'>');
                strncpy(buff,i+1,j-i-1);
                fprintf(f2,"%s\n\n",buff);
                memset(buff,0,100);
    }
    return 0;
}
int FB2_parse(char *str,FILE *f1, FILE *f2 )
{
    char *i,*j,*k;
    char buff[2000];
    memset(buff,0,2000);
    while((strstr(str,"</body>"))==NULL)
    {
        fgets(str,2000,f1);
        FB2_constr(str);
        //printf("%s\n______\n",str);
        j=str;
        i=strstr(str,"<p>");
        while(i!=NULL && j!=NULL)
        {
            if(i!=NULL)
            {
                j=strstr(i,"</p>");
                if((j==(i+2))||(j==i+3))
                {
                    strcpy(buff,"\n");
                }
                else {strncpy(buff,i+3,j-i-3);}
                //printf("buf=%s\n",buff);
                fprintf(f2,"%s\n",buff);
                memset(buff,0,2000);
                i=strstr(j,"<p>");
                //printf("%s\n______\n",j);
            }
        }
        j=str;
        i=strstr(str,"<v>");
        while(i!=NULL && j!=NULL)
        {
            //printf("%s\n______\n",j);
            //printf("%s\n______\n",i);
            if(i!=NULL)
            {
                j=strstr(i,"</v>");
                if((j==(i+2))||(j==i+3))
                {
                    strcpy(buff,"\n");
                }
                else {strncpy(buff,i+3,j-i-3);}
                //printf("buf=%s\n",buff);
                fprintf(f2,"%s\n",buff);
                memset(buff,0,2000);
                i=strstr(j,"<v>");
                //printf("%s\n______\n",j);
            }
        }
    }
}
int FB2_constr(char *str)
{
    int l=0,flag=0,k;
    char *i,*j;
    char buff[50],constr[50];
    FILE *f3;
    i=strchr(str,'<');
    while(i!=NULL)
    {
        flag=0;
        j=strchr(i,'>');
        l=j-i;
        memset(buff,0,50);
        memset(constr,0,50);
        strncpy(constr,i+1,l-1);
        //printf("constr=%s__\n",constr);
        f3=fopen("constrfb2.txt","r");
        while(fgets(buff,50,f3)!=NULL)
        {
            k=strlen(buff);
            if(buff[k-1]=='\n') buff[k-1]='\0';
            if((strcmp(buff,constr))==0)
            {
                //printf("y=%s__\n",constr);
                flag=1;
                memset(buff,0,50);
                break;
            }
        }
        if (flag==0)
        {
            //printf("n=%s__\n",constr);
            strcpy(i,j+1);
            //printf("constr=%s__\n",constr);
            flag=0;
            i=strchr(j,'<');
            continue;
        }
        i=strchr(j,'<');
        fclose(f3);
    }

    return 0;
}
