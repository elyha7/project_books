#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

int TxtToRtf(char *source, char *target,char *font, int color[2],char *fsize)
{
    /*if (font==NULL)
    {
        char defaultfont[10]="Arial"
        printf("\n____________\n");

    }*/
    printf("\n____________\n");
    printf("font=%s",font);
    printf("\n_____red=%i  green=%i  blue=%i  ________\n",color[0],color[1],color[2]);
    FILE *f1, *f2;
    if( (f1=fopen(source,"r")) == 0 )
    {
     printf("2 can't open source file\n");
     exit(1);
    }
    if( (f2=fopen(target,"w")) == 0 )
    {
     printf("2 can't open target file\n");
     exit(1);
    }
    char *buff;
    buff=malloc(3000);
    char *i,*j;
    /* head of rtf text*/
    fprintf(f2,"{\\rtf1\\ansi\\ansicpg1251\\deff0\\nouicompat\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset0 %s;}}\n",font);
    fprintf(f2,"{\\colortbl ;\\red%i\\green%i\\blue%i;}\n",color[0],color[1],color[2]);
    fprintf(f2,"{\\*\\generator Riched20 6.3.9600}\\viewkind4\\uc1 \n",color[0],color[1],color[2]);
    /* main text*/
    while((fgets(buff,3000,f1))!=NULL)
    {
        if((strstr(buff,"http")))
        {
            for(i=buff;(*i)!='\n';i++)
            {
                if((*i=='{')||(*i=='}'))
                    *i=' ';
            }
        }
        //printf("buff=%s",buff);
        fprintf(f2,"\\pard\\cf1\\sb100\\sa100\\sl330\\fs24\\f0\\fs%s %s\\par\n",fsize,buff);
    }
    fprintf(f2,"}\n");
    free(buff);
    fclose(f1);
    fclose(f2);
    return 0;
}
