#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /*! Open file button
     * Opens ordinary windows explorer to find file for converting
     */
    void on_Openfile_clicked();
    /*! Run button.
     * Sends the data from other buttons to starter function.
     */
    void on_commandLinkButton_clicked();
    /*! Save button
     * Choose a destination folder for converted file.
     */
    void on_choosedest_clicked();
    /*! Combobox button
     * Gets information from combobox and decides whether hide or show some other buttons.
     */
    void on_comboBox_activated(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QFileSystemModel *dirmodel;
    QFileSystemModel *filemodel;
    QString destname;
    QString filename;

};

#endif // MAINWINDOW_H
