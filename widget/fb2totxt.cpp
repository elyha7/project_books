#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <unistd.h>
#include <textfunctions.h>

/** converts fb2 to txt
 * Parses fb2 text. removes special hml constructions and saves the data in txt file.
 */
int FB2toTXT(char *fname,char *target)
{
    printf("filename=%s\n",fname);
    FILE *f1;
    FILE *f2;
    if( (f1=fopen(fname,"r")) == 0 )
    {
     printf(" 1 can't open source file\n");
     exit(1);
    }
    if( (f2=fopen(target,"w")) == 0 )
    {
     printf("1 can't open target file\n");
     exit(1);
    }
    char *str,*constr;
    str=(char*) malloc(3000);
    constr=(char*) malloc(100);
    char *i,*j,*k;
    int l,p=0,p1=0,author=0,title=0,flag=0;
    do
    {
        fgets(str,3000,f1);
        if (author==0)
        {
            if((i=strstr(str,"<author>"))!=NULL)
            {
                FB2_getauthor(str,f1,f2);
                author=1;
            }
        }
        if (title==0)
        {
            if((i=strstr(str,"<book-title>"))!=NULL)
            {
                FB2_gettitle(str,f2);
                title=1;
            }
        }
        if(flag==0)
        {
            if((k=strstr(str,"<body>"))!=NULL)
            {
                flag=1;
            }
        }
        else
        {
            flag=2;
            p1=FB2_parse(str,f1,f2);
        }
    p++;
    }while(flag!=2);
    ender:
    free(str);
    free(constr);
    fclose(f1);
    fclose(f2);
    return 0;
}
/** Get text's author
 * Takes author's name from special fb2 constructions if he is presented.
 *
 */
int FB2_getauthor(char *str,FILE *f1, FILE *f2 )
{
    char *i,*j;
    char buff[50],tofile[100];
    sprintf(tofile,"Author: ");
    memset(buff,0,50);
    while((strstr(str,"</author>"))==NULL)
    {
        fgets(str,2000,f1);
        if((i=strstr(str,"<first-name>"))!=NULL)
        {
            if((j=strstr(i,"</first-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s ",tofile,buff);
                memset(buff,0,50);
            }
        }
        if((i=strstr(str,"<middle-name>"))!=NULL)
        {
            if((j=strstr(i,"</middle-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s ",tofile,buff);
                memset(buff,0,50);
            }
        }
        if((i=strstr(str,"<last-name>"))!=NULL)
        {
            if((j=strstr(i,"</last-name>"))!=NULL)
            {
                i=strchr(i,'>');
                strncpy(buff,i+1,j-i-1);
                sprintf(tofile,"%s%s",tofile,buff);
                memset(buff,0,50);
            }
        }
    }
    if((strlen(tofile))>10)
        fprintf(f2,"%s\n",tofile);
    return 0;
}
/** Get book's title.
 * Takes book's title from fb2 constructions if it is presented.
 */
int FB2_gettitle(char *str,FILE *f2 )
{
    char *i,*j;
    char buff[100];
    memset(buff,0,100);
    j=strstr(str,"<book-title>");
    i=strchr(j+1,'>');
    if((j=strstr(i,"</book-title>"))!=NULL)
    {
                strncpy(buff,i+1,j-i-1);
                fprintf(f2,"%s\n\n",buff);
                memset(buff,0,100);
    }
    return 0;
}
/** Fb2 text parse
 * Remove's hml constructions and converts string to an ordinary txt string.
 */
int FB2_parse(char *str,FILE *f1, FILE *f2 )
{
    char *i,*j,*k,*m;
    char *buff;
    buff=(char*) malloc(3000);
    while(1)
    {
        fgets(str,3000,f1);
        if((strstr(str,"</body>"))!=NULL)
        {
            return 5;
        }
        FB2_constr(str);
        j=str;
        i=strstr(str,"<p>");
        m=strstr(str,"<v>");
        if((m==NULL) && (i==NULL))
        {
            fprintf(f2,"%s",str);
        }
        while(i!=NULL && j!=NULL)
        {
            if(i!=NULL)
            {
                j=strstr(i,"</p>");
                if((j==(i+2))||(j==i+3))
                {
                    strcpy(buff,"\n");
                }
                else
                    {
                        if(j==NULL)
                        {
                            fgets(str,3000,f1);
                            j=strstr(i,"</p>");
                            strncpy(buff,str,j-str);
                            fprintf(f2,"%s\n",buff);
                            break;
                        }
                        else{strncpy(buff,i+3,j-i-3);}
                    }
                fprintf(f2,"%s\n",buff);
                memset(buff,0,3000);
                i=strstr(j,"<p>");
            }
        }
        j=str;
        i=strstr(str,"<v>");
        while(i!=NULL && j!=NULL)
        {
            if(i!=NULL)
            {
                j=strstr(i,"</v>");
                if((j==(i+2))||(j==i+3))
                {
                    strcpy(buff,"\n");
                }
                else
                {
                    if(j==NULL)
                        {
                            fgets(str,3000,f1);
                            j=strstr(i,"</v>");
                            strncpy(buff,str,j-str);
                            fprintf(f2,"%s\n",buff);
                            break;
                        }
                    else{strncpy(buff,i+3,j-i-3);}
                }
                fprintf(f2,"%s\n",buff);
                memset(buff,0,3000);
                i=strstr(j,"<v>");
                //printf("%s\n______\n",j);
            }
        }
    }
    free(buff);
    return 5;
}
/** Decides whether construction must be removed.
 *
 */
int FB2_constr(char *str)
{
    int l=0,flag=0,k;
    char *i,*j;
    char buff[50],constr[50];
    FILE *f3;
    i=strchr(str,'<');
    while(i!=NULL)
    {
        flag=0;
        j=strchr(i,'>');
        l=j-i;
        memset(buff,0,50);
        memset(constr,0,50);
        strncpy(constr,i+1,l-1);
        f3=fopen("constrfb2.txt","r");
        if(f3==0){exit(5);}
        while(fgets(buff,50,f3)!=NULL)
        {
            k=strlen(buff);
            if(buff[k-1]=='\n') buff[k-1]='\0';
            if((strcmp(buff,constr))==0)
            {
                //printf("y=%s__\n",constr);
                flag=1;
                memset(buff,0,50);
                break;
            }
        }
        if (flag==0)
        {
            strcpy(i,j+1);
            flag=0;
            i=strchr(j,'<');
            fclose(f3);
            continue;
        }
        i=strchr(j,'<');
        fclose(f3);
    }
    //printf("\n______%s_____\n",str);
    return 0;
}
