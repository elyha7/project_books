#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <textfunctions.h>
/** Txt to fb2 converting
 * Adds fb2 constructions to clean txt format text.
 */
int TxtTofb2(char *source, char *target)
{
    FILE *f1, *f2, *f3;
    if( (f1=fopen(source,"r")) == 0 )
    {
     printf("2 can't open source file\n");
     exit(1);
    }
    if( (f2=fopen(target,"w")) == 0 )
    {
     printf("2 can't open target file\n");
     exit(1);
    }
    char *str,*buff;
    str=(char*)malloc(1000);
    buff=(char*)malloc(50);
    char *i,*j;
    int k;
    /* head of rtf text*/
    fprintf(f2,"<?xml version=\"1.0\" encoding=\"Windows-1251\"?>\n");
    fprintf(f2,"<FictionBook xmlns=\"http://www.gribuser.ru/xml/fictionbook/2.0\" xmlns:l=\"http://www.w3.org/1999/xlink\">\n");
    fprintf(f2,"<description>\n <title-info>\n </title-info>\n <document-info>\n </document-info>\n</description>\n");
     // main text
     fprintf(f2,"<body>\n <section>\n");
//fprintf(f2,"  <font face='%s' color='%s'>\n",font,color);
    while((fgets(str,1000,f1))!=NULL)
    {
        //printf("\n________\n%s",str);
        k=strlen(str);
        if(str[k-1]=='\n') str[k-1]='\0';
        f3=fopen("constrfb2n.txt","r");
        while(fgets(buff,50,f3)!=NULL)
        {
            k=strlen(buff);
            if(buff[k-1]=='\n') buff[k-1]='\0';
            if((j=strstr(str,buff))!=NULL)
            {
                strcpy(j,j+strlen(buff));
            }
        }
        fclose(f3);
        //printf("buff=%s",buff);
        fprintf(f2,"   <p>%s</p>\n",str);
    }
    fprintf(f2," </section>\n</body>\n</FictionBook>",str);
    free(str);
    free(buff);
    fclose(f1);
    fclose(f2);
    return 0;
}
