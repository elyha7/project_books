#ifndef TEXTFUNCTIONS_H_INCLUDED
#define TEXTFUNCTIONS_H_INCLUDED

int FB2_getauthor(char *str,FILE *f1, FILE *f2 );
int FB2_gettitle(char *str,FILE *f2 );
int FB2_parse(char *str,FILE *f1, FILE *f2 );
int FB2_constr(char *str);
int RtfToTxt_constr(char *constr, FILE *f2);
int Rtf_getfont(char *str, char *font);
int Rtf_getcolor(char *str, int color[2]);
int TxtToRtf(char *source, char *target,char *font, int color[2],char *fsize);
int RTFtoTXT(char *fname,char *target);
int FB2toTXT(char *fname,char *target);
int TxtTofb2(char *source, char *target);
int starter(int argc,int mode, char* argv[]);

#endif // TEXTFUNCTIONS_H_INCLUDED
