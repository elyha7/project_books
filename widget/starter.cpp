#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <unistd.h>
#include <textfunctions.h>
#include <QDebug>
/** Starts main functions
 * Called in widget. Chooses which mode to use and starts certain functions.
 */
int starter(int argc,int mode, char* argv[])
{
    qDebug() << "STARTER\n";
    qDebug() << mode << '\n';
    qDebug() << argv[2] << '\n';
    qDebug() << argv[3] << '\n';
    qDebug() << argv[4] << '\n';
    qDebug() << argv[5] << '\n';
    int color[2];
    color[0]=0;color[1]=0;color[2]=0;
    char *buff="buff.txt";
    switch(mode)
    {
        case 1:
            qDebug() << "MODE1\n";
            RTFtoTXT(argv[4],buff);
            TxtTofb2(buff,argv[5]);
            printf("Mode 1 success\n");
            goto ender;
        case 2:
            qDebug() << "MODE2\n";
            FB2toTXT(argv[4],buff);
            TxtToRtf(buff,argv[5],argv[2],color,argv[3]);
            //TxtToRtf(buff,argv[5],"Arial",color,"30");
            printf("Mode 2 success\n");
            goto ender;
        case 3:
            qDebug() << "MODE3\n";
            FB2toTXT(argv[4],argv[5]);
            goto ender;
        case 4:
            qDebug() << "MODE4\n";
            RTFtoTXT(argv[4],argv[5]);
            goto ender;
    }
    ender:
    return 0;
}
