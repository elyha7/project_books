#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Qtcore"
#include "QtGui"
#include <QMessageBox>
#include <QString>
#include <iostream>
#include <QFileDialog>
#include <QTreeView>
#include <QWidget>
#include <cstdlib>
#include <textfunctions.h>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
/*! Open file button
 * Opens ordinary windows explorer to find file for converting
 */
void MainWindow::on_Openfile_clicked()
{
    filename=QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "C://",
                "All files (*.*);;Fb2 files (*.fb2);;Rtf files (*.rtf)"
                );

}
/*! Run button.
 * Sends the data from other buttons to starter function.
 */
void MainWindow::on_commandLinkButton_clicked()
{
    QString mode, font;
    int size,argc=5;
    mode = ui->comboBox->currentText();
    font = ui->comboBox_2->currentText();
    size= ui->Sizebox->value();
    size=size*2;
    QString size1 = QString::number(size);
    qDebug() << "mode: " << mode << '\n';
    qDebug() << "font: " << font << '\n';
    qDebug() << "size: " << size1 << '\n';
    qDebug() << "source: " << filename <<'\n';
    qDebug() << "dest: " << destname <<'\n';
    char *argv[5];
    qDebug() << "_________\n";
    QByteArray fo, fi, de, si;
    fo = font.toLatin1();
    fi = filename.toLatin1();
    de = destname.toLatin1();
    si = size1.toLatin1();
    argv[2] = fo.data();
    argv[3] = si.data();
    argv[4] = fi.data();
    argv[5] = de.data();
    //qDebug() << argv[3];
    if(mode == "Fb2->Rtf")
    {
        starter(argc,2,argv);
    }
    if(mode == "Rtf->Fb2")
    {
        starter(argc,1,argv);
    }
    if(mode == "Fb2->Txt")
    {
        starter(argc,3,argv);
    }
    if(mode == "Rtf->Txt")
    {
        starter(argc,4,argv);
    }
}
/*! Save button
 * Choose a destination folder for converted file.
 */
void MainWindow::on_choosedest_clicked()
{
    destname = QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                "C://",
                "All files (*.*);;Fb2 files (*.fb2);;Rtf files (*.rtf);;Txt files(*.txt)"
                );
}
/*! Combobox button
 * Gets information from combobox and decides whether hide or show some other buttons.
 */
void MainWindow::on_comboBox_activated(const QString &arg1)
{
    QString mode, font;
    mode = ui->comboBox->currentText();
    if(mode == "Fb2->Rtf")
    {
        ui->comboBox_2->setVisible(true);
        ui->Sizebox->setVisible(true);
        ui->label->setVisible(true);
        ui->label_2->setVisible(true);
    }
    else
    {
        ui->comboBox_2->setVisible(false);
        ui->Sizebox->setVisible(false);
        ui->label->setVisible(false);
        ui->label_2->setVisible(false);
    }
}
