#-------------------------------------------------
#
# Project created by QtCreator 2016-05-16T22:10:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = widget
TEMPLATE = app


SOURCES +=\
        mainwindow.cpp \
    main.cpp \
    starter.cpp \
    rtftotxt.cpp \
    txttofb2.cpp \
    txttortf.cpp \
    fb2totxt.cpp

HEADERS  += mainwindow.h \
    textfunctions.h

FORMS    += mainwindow.ui
